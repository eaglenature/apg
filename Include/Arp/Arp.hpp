#pragma once

#include <Arp/Amorphous/WithAssociatedRings.hpp>
#include <Arp/Elliptical/WithNearbyFragments.hpp>
#include <Arp/Miscellaneous/Messier82.hpp>
#include <Arp/Multiple/WithWindEffects.hpp>
#include <Arp/Spiral/LowSurfaceBrightness.hpp>

namespace Arp
{
}
