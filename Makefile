################################################################################################
# Main Makefile
################################################################################################
include Makefile.include


all: envinfo \
	 examples \
	 perftest \
	 unittest

hello-config-test:
	@echo HELLO_WORLD_FROM_CONFIG value is: $(HELLO_WORLD_FROM_CONFIG)
	@echo FILE_LOGGER_FROM_CONFIG value is: $(FILE_LOGGER_FROM_CONFIG)

envinfo:
	@echo ----------------------------------------------------------------------------------------
	@echo Platform:      $(PLATFORM)
	@echo Project root:  $(PROJECT_ROOT)
	@echo Compiler:      $(CXX)

examples:
	@echo ----------------------------------------------------------------------------------------
	@echo /Build examples:
	@echo
	@mkdir -p bin
	@make --makefile=Examples/Makefile --no-print-directory
	@echo

perftest:
	@echo ----------------------------------------------------------------------------------------
	@echo /Build performance tests:
	@echo
	@mkdir -p bin
	@echo Perftest
	@echo

unittest:
	@echo ----------------------------------------------------------------------------------------
	@echo /Build unit tests:
	@echo
	@mkdir -p bin
	@make --makefile=Tests/Makefile --no-print-directory
	@echo

install:
	@echo Install

uninstall:
	@echo Uninstall

clean:
	rm -rf bin/*.o
	rm -Rf bin
