#include <iostream>
#include <Arp/Arp.hpp>

int main()
{
#ifdef _FILE_LOGGER_    
    std::cout << "[file] A constellation is a group of stars that forms an imaginary outline\n"
              << "or meaningful pattern on the celestial sphere..." << std::endl;
#else
    std::cout << "[cout] A constellation is a group of stars that forms an imaginary outline\n"
              << "or meaningful pattern on the celestial sphere..." << std::endl;
#endif              

}
